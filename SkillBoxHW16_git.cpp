#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <time.h>

int main()
{
	struct tm buf;
	time_t t = time(NULL);

	localtime_s(&buf, &t);
	const int n = 5;
	int array[n][n];
	int summ = 0;
	buf.tm_mday;
	std::cout << buf.tm_mday << '\n';
	int index = buf.tm_mday % n;
	std::cout << index << '\n';

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			array[i][j] = i + j;
		}
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}

	std::cout << "Sum of row " << index << ": ";

	for (int i = 0; i < n; i++)
	{
		summ += array[index][i];
	}
	std::cout << summ << "\n";

	return 0;
}
